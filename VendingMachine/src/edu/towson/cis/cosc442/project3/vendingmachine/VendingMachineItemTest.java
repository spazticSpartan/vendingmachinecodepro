package edu.towson.cis.cosc442.project3.vendingmachine;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class VendingMachineItemTest {
	
	//declare VendingMachine object
	private VendingMachine vendingMachine1;
	private VendingMachineItem vendingMachineItem1;
	private VendingMachineItem vendingMachineItem2;


	@Test
	public void testGetNamePriceZero() {
		VendingMachine vendingMachine1 = new VendingMachine();
			vendingMachineItem1 = new VendingMachineItem("Chips", 2.99);
			vendingMachine1.addItem(vendingMachineItem1, "A");
			assertEquals("Chips", vendingMachineItem1.getName());
		}
	
	
	
	@Test
	//creates a vending machine item named Chips, then checks if that item name can be verified through accessEquals with .getName()
	public void testGetName() {
		vendingMachine1 = new VendingMachine();
		
		vendingMachineItem1 = new VendingMachineItem("Chips", 2.99);
		vendingMachine1.addItem(vendingMachineItem1, "A");
		assertEquals("Chips", vendingMachineItem1.getName());

	}
	
	@Test
	//creates a vending machine item named Chips, then checks if that item price can be verified through accessEquals with .getPrice()
	public void testGetPrice() {
		VendingMachine vendingMachine1 = new VendingMachine();
		vendingMachineItem1 = new VendingMachineItem("Chips", 2.99);
		vendingMachine1.addItem(vendingMachineItem1, "A");
		assertEquals(2.99, vendingMachineItem1.getPrice(), 0);
		try {vendingMachineItem2 = new VendingMachineItem("FakeItem", -2.99);}
		catch(VendingMachineException e){
		}

	}
}
