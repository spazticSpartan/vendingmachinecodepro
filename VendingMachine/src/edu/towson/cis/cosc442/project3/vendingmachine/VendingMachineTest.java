package edu.towson.cis.cosc442.project3.vendingmachine;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class VendingMachineTest {

	//declares VendingMachine object and VendingMachineItem Object
	private VendingMachine vendingMachine1;
	private VendingMachine vendingMachine2;
	private VendingMachineItem vendingMachineItem1;
	private VendingMachineItem vendingMachineItem2;
	
	@Test 
	//creates a vending machine and a vending machine item named Chips, adds item Chips into machine,
	//then checks if that item name can be verified as in machine through accessEquals with .getName()
	public void testaddItem() {
		vendingMachine1 = new VendingMachine();
		vendingMachineItem1 = new VendingMachineItem("Chips", 2.99);
		vendingMachine1.addItem(vendingMachineItem1, "A");
		assertEquals("Chips", vendingMachineItem1.getName());
		try {vendingMachine1.addItem(vendingMachineItem1, "A");}
		catch(VendingMachineException e){
		}
	}
	
	@Test
	public void testRemoveItem() {
		vendingMachine1 = new VendingMachine();
		vendingMachineItem1 = new VendingMachineItem("Chips", 2.99);
		vendingMachine1.addItem(vendingMachineItem1, "A");
		vendingMachine1.removeItem("A");
		assertEquals("Chips", vendingMachineItem1.getName());
		try {vendingMachine1.removeItem("A");}
		catch(VendingMachineException e){
		}
	}
	
	@Test
	//creates a vending machine, inserts $5 into machine,
	//then checks if that balance can be verified as in machine through accessEquals with .getBalance()
	public void testInsertMoney() {
		vendingMachine1 = new VendingMachine();
		vendingMachine1.insertMoney(5.00);
		assertEquals(5.00, vendingMachine1.getBalance(), 0);
		try {vendingMachine1.insertMoney(-4.00);}
		catch(VendingMachineException e) {
		}
	}
	
	@Test
	//same as testInsertMoney check, just checking it at 0
	public void testGetBalance() {
		vendingMachine1 = new VendingMachine();
		assertEquals(0.00, vendingMachine1.getBalance(), 0);
	}
	
	@Test
	public void testMakePurchase() {
		vendingMachine1 = new VendingMachine();
		vendingMachine1.balance = Double.MAX_VALUE;
		vendingMachineItem1 = new VendingMachineItem("Chips", 2.99);
		vendingMachine1.addItem(vendingMachineItem1, "A");
		vendingMachine1.makePurchase("A");
		
		vendingMachine1.balance = 1.50;
		vendingMachine1.addItem(vendingMachineItem1, "A");
		vendingMachine1.makePurchase("A");

		vendingMachine1.balance = 2.99;
		vendingMachine1.makePurchase("B");
	}
	
	
	
	@Test
	//creates a vending machine, inserts $5 into machine, tells machine to return change
	//then checks if that balance can be verified as back to 0 in machine through accessEquals with .getBalance()
	public void testReturnChange() {
		vendingMachine1 = new VendingMachine();
		vendingMachine1.insertMoney(5.00);
		vendingMachine1.returnChange();
		assertEquals(0.00, vendingMachine1.getBalance(), 0);
	}	
	
	@Test
	public void testGetSlotIndex() {
		vendingMachine1 = new VendingMachine();
		vendingMachineItem1 = new VendingMachineItem("Chips", 2.99);
		vendingMachine1.addItem(vendingMachineItem1, "D");
		vendingMachine1.addItem(vendingMachineItem1, "C");
		vendingMachine1.addItem(vendingMachineItem1, "B");
		vendingMachine1.addItem(vendingMachineItem1, "A");
		try {vendingMachine1.getSlotIndex("E");}
		catch(VendingMachineException e){
		}
	}
	
}
